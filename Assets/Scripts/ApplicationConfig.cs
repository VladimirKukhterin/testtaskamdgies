using UnityEngine;
using System.IO;
using UniRx;

public class ApplicationConfig
{
    private ReactiveProperty<string> _serverAddress = new ReactiveProperty<string>();
    public ReactiveProperty<string> ServerAddress => _serverAddress;

    private ReactiveProperty<int> _serverPort = new ReactiveProperty<int>();
    public ReactiveProperty<int> ServerPort => _serverPort;

    private ReactiveProperty<string> _videoStream = new ReactiveProperty<string>();
    public ReactiveProperty<string> VideoStream => _videoStream;

    public ApplicationConfig()
    {
        LoadConfig();        
        Observable.Merge(
            _serverAddress.AsUnitObservable(), 
            _serverPort.AsUnitObservable(),
            _videoStream.AsUnitObservable()
            ).Subscribe(_ => saveConfig());
    }

    public void LoadConfig()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, "Config.txt");

        if (File.Exists(filePath))
        {
            string[] configLines = File.ReadAllLines(filePath);

            foreach (string line in configLines)
            {
                if (line.StartsWith("����� �������:"))
                {
                    string address = line.Replace("����� �������:", string.Empty).Trim();
                    _serverAddress.Value = address;
                }
                else if (line.StartsWith("����:"))
                {
                    if (int.TryParse(line.Replace("����:", string.Empty).Trim(), out int port))
                    {
                        _serverPort.Value = port;
                    }
                    else
                    {
                        Debug.LogError("������������ ���� � �������");
                    }
                }
                else if (line.StartsWith("����� ����� ������:"))
                {
                    string address = line.Replace("����� ����� ������:", string.Empty).Trim();
                    _videoStream.Value = address;
                }
            }
        }
        else
        {
            Debug.LogError("���� ������������ �� ������!");
        }
    }

    private void saveConfig()
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, "Config.txt");

        using (StreamWriter writer = new StreamWriter(filePath))
        {
            writer.WriteLine("����� �������: " + _serverAddress.Value);
            writer.WriteLine("����: " + _serverPort.Value);
            writer.WriteLine("����� ����� ������: " + _videoStream.Value);
        }
    }
}