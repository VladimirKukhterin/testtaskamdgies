using UnityEngine;

public class ApplicationContext : MonoBehaviour
{
    private ApplicationConfig _applicationConfig = new ApplicationConfig();
    public ApplicationConfig ApplicationConfig => _applicationConfig;
    
}
