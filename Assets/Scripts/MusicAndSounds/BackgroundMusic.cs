using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    [SerializeField]
    private AudioClip _bacgroundMusic;

    private void Start()
    {
        MusicPlayer.Play(_bacgroundMusic);
    }    
}
