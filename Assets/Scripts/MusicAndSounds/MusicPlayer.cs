using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MusicPlayer
{
    public static bool IsMuted = false;
    private static AudioSource _audioSource;
   
    public static void Play(AudioClip music)
    {

        GameObject musicGameObject = new GameObject("Music");
        _audioSource = musicGameObject.AddComponent<AudioSource>();
        _audioSource.clip = music;
        _audioSource.loop = true;
        _audioSource.Play();
        _audioSource.mute = IsMuted;        
    }

    public static void SetMuted(bool isOn)
    {
        _audioSource.mute = isOn;
        IsMuted = isOn;
    }

    public static void SetVolume(float vol)
    {
        _audioSource.volume = vol;
    }
}
