using UnityEngine;
using UnityEngine.UI;

public class MuteMusicPlayer : MonoBehaviour
{
    [SerializeField] 
    private Toggle _musicToggle;
    // Start is called before the first frame update
    private void Awake()
    {
        _musicToggle.onValueChanged.AddListener(delegate { OnToggleValueChanged(_musicToggle); });
    }

    public void OnToggleValueChanged(Toggle change)
    {
            MusicPlayer.SetMuted(!change.isOn);
    }
}
