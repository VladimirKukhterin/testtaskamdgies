using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteSoundPlayer : MonoBehaviour
{
    [SerializeField]
    private Toggle _soundToggle;
    // Start is called before the first frame update
    private void Awake()
    {       
        _soundToggle.onValueChanged.AddListener(delegate { OnToggleValueChanged(_soundToggle); });
    }

    public void OnToggleValueChanged(Toggle change)
    {       
        SoundPlayer.IsMute = !change.isOn;
    }
}
