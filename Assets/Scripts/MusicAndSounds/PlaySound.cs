using UnityEngine;

public class PlaySound : MonoBehaviour
{
    public void play(AudioClip _sound)
    {
        SoundPlayer.Play(_sound);
    }
}
