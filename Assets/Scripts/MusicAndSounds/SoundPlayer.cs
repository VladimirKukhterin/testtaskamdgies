using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public static bool IsMute = false;
    private static float _volume;

    public static void Play(AudioClip sound)
    {
        if (IsMute)
        {
            return;
        }
        GameObject soundGameobject = new GameObject("Sound");
        AudioSource audioSource = soundGameobject.AddComponent<AudioSource>();
        audioSource.volume = _volume;
        audioSource.PlayOneShot(sound);
        soundGameobject.AddComponent<DestroyObject>().time = sound.length;
    }

     public static void SetVolume(float vol)
    {
        _volume = vol;
    }
}
