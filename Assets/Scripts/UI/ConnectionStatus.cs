using UnityEngine;
using UniRx;
using TMPro;
using UnityEngine.UI;

public class ConnectionStatus : MonoBehaviour
{
    [SerializeField]
    private WebSocketClient _webSocketClient;
    [SerializeField]
    private Image _lampImage;
    [SerializeField]
    private TMP_Text _textConnectionLost;

    // Start is called before the first frame update
    void Start()
    {      
        _webSocketClient.ConnectionStatus.Subscribe(value => _lampImage.color = value ? Color.green : Color.red).AddTo(this);
        _webSocketClient.ConnectionStatus.Subscribe(value => _textConnectionLost.enabled = !value).AddTo(this);
    }    
}
