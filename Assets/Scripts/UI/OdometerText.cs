using UnityEngine;
using TMPro;
using UniRx;
using System.Threading.Tasks;
using System;

[RequireComponent(typeof(TMP_Text))]
public class OdometerText : MonoBehaviour
{
    [SerializeField] 
    private WebSocketClient _webSocketClient;
    [SerializeField]
    private Animator _animator;
    void Start()
    {
        TMP_Text odometerText = GetComponent<TMP_Text>();
        _webSocketClient.OdometerValue.First().Subscribe(x => odometerText.text = $"Odometer: {x}");
        _webSocketClient.OdometerValue.Skip(1).Subscribe(async x =>
        {
            _animator.SetTrigger("changeNumber");
            await Task.Delay(TimeSpan.FromMilliseconds(500));
            odometerText.text = $"Odometer: {x}";
        }).AddTo(this);
    }

}
