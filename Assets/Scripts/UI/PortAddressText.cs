using UnityEngine;
using TMPro;
using UniRx;

public class PortAddressText : MonoBehaviour
{
    [SerializeField]
    private ApplicationContext _applicationContext;
    void Start()
    {
        TMP_InputField portAdresses = GetComponent<TMP_InputField>();
        _applicationContext.ApplicationConfig.ServerPort.Subscribe(value => portAdresses.text = $"{value}").AddTo(this);
        portAdresses.onEndEdit.AsObservable().Subscribe(value => _applicationContext.ApplicationConfig.ServerPort.Value = int.Parse(value));
    }
}
