using UnityEngine;
using TMPro;
using UniRx;
using UnityEngine.UI;

public class RandomToggle : MonoBehaviour
{
    [SerializeField]
    private WebSocketClient _webSocketClient;
    void Start()
    {
        Toggle toggle  = GetComponent<Toggle>();
        _webSocketClient.RandomValue.Subscribe(value => toggle.isOn = value).AddTo(this);
    }
}
