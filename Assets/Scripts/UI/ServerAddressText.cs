using UnityEngine;
using TMPro;
using UniRx;

public class ServerAddressText : MonoBehaviour
{
    [SerializeField]
    private ApplicationContext _applicationContext;
    void Start()
    {
        TMP_InputField serverAdresses = GetComponent<TMP_InputField>();
        _applicationContext.ApplicationConfig.ServerAddress.Subscribe(value => serverAdresses.text = $"{value}").AddTo(this);
        serverAdresses.onEndEdit.AsObservable().Subscribe(value => _applicationContext.ApplicationConfig.ServerAddress.Value = value);
    }
}
