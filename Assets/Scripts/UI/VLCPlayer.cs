using LibVLCSharp;
using System;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class VLCPlayer : MonoBehaviour
{
    [SerializeField] 
    private RawImage _rawImage;
    [SerializeField]
    private ApplicationContext _applicationContext;
    private LibVLC _libVLC;
    private MediaPlayer _mediaPlayer;
    private Texture2D tex = null;
    private bool playing;

    void Awake()
    {
        TextureHelper.FlipTextures(transform);

        Core.Initialize(Application.dataPath);

        _libVLC = new LibVLC(enableDebugLogs: true);

        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);

        _applicationContext.ApplicationConfig.VideoStream.Subscribe(_ => Reload());
    }

    void OnDisable()
    {
        _mediaPlayer?.Stop();
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;

        _libVLC?.Dispose();
        _libVLC = null;
    }

    private void Reload()
    {
        if (_mediaPlayer != null)
        {
            _mediaPlayer.Media = new Media(new Uri(_applicationContext.ApplicationConfig.VideoStream.Value));
            _rawImage.texture = Texture2D.blackTexture;
        }
    }

    public void PlayPause()
    {
        Debug.Log("[VLC] Toggling Play Pause !");
        if (_mediaPlayer == null)
        {
            _mediaPlayer = new MediaPlayer(_libVLC);
        }
        if (_rawImage.enabled)
        {
            _mediaPlayer.Pause();
            SoundPlayer.IsMute = (false);
            MusicPlayer.SetMuted(false);
            _rawImage.enabled = false;
        }
        else
        {
            playing = true;

            if (_mediaPlayer.Media == null)
            {
                // playing remote media
                Reload();
            }
            _mediaPlayer.Play();
            _mediaPlayer.SeekTo(TimeSpan.Zero, true);
            SoundPlayer.IsMute = (true);
            MusicPlayer.SetMuted(true);
            _rawImage.enabled = true;
        }
    }

    void Update()
    {
        if (!playing || !_mediaPlayer.SelectedProgram.HasValue) return;

        if (tex == null)
        {
            // If received size is not null, it and scale the texture
            uint i_videoHeight = 0;
            uint i_videoWidth = 0;

            _mediaPlayer.Size(0, ref i_videoWidth, ref i_videoHeight);
            var texptr = _mediaPlayer.GetTexture(i_videoWidth, i_videoHeight, out bool updated);
            if (i_videoWidth != 0 && i_videoHeight != 0 && updated && texptr != IntPtr.Zero)
            {
                tex = Texture2D.CreateExternalTexture((int)i_videoWidth,
                    (int)i_videoHeight,
                    TextureFormat.RGBA32,
                    false,
                    true,
                    texptr);
                _rawImage.texture = tex;
            }
        }
        else if (tex != null)
        {
            var texptr = _mediaPlayer.GetTexture((uint)tex.width, (uint)tex.height, out bool updated);
            if (updated)
            {
                tex.UpdateExternalTexture(texptr);
            }
            _rawImage.texture = tex;
        }
    }
}
