using UnityEngine;
using TMPro;
using UniRx;

public class VideoStreamText: MonoBehaviour
{
    [SerializeField]
    private ApplicationContext _applicationContext;
    void Start()
    {
        TMP_InputField videoStreamAddress = GetComponent<TMP_InputField>();
        _applicationContext.ApplicationConfig.VideoStream.Subscribe(value => videoStreamAddress.text = $"{value}").AddTo(this);
        videoStreamAddress.onEndEdit.AsObservable().Subscribe(value => _applicationContext.ApplicationConfig.VideoStream.Value = value);
    }


}
