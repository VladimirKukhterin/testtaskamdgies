using UnityEngine;
using UnityEngine.UI;

public class VolumeValue : MonoBehaviour
{
    private float _volume = 1f;
    private Slider _slider;

    private enum TypePlayer 
    {
        MusicPlayer,
        SundPlayer
    }

    [SerializeField]
    private TypePlayer _typePlayer;

    private void Start()
    {
        _slider = GetComponent<Slider>();
    }

    public void Update()
    {
        _volume = _slider.value;
        switch (_typePlayer)
        {
            case TypePlayer.MusicPlayer:
                MusicPlayer.SetVolume(_volume);
                break;
            case TypePlayer.SundPlayer:
                SoundPlayer.SetVolume(_volume);
                break;
            default:
                MusicPlayer.SetVolume(_volume);
                break;
        }
        
    }
}
