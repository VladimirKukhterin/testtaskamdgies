using UnityEngine;

public class WindowsNavigation : MonoBehaviour
{
    public void NavigatePanel(GameObject hideWindow, GameObject showWindow)
    {
        HideWindow(hideWindow);
        ShowWindow(showWindow);
    }

    private void HideWindow(GameObject hideWindow)
    {
        hideWindow.SetActive(false);
    }

    private void ShowWindow(GameObject ShowWindow)
    {
        ShowWindow.SetActive(true);
    }
}
