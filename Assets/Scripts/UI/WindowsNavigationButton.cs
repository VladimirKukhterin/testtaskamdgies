using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class WindowsNavigationButton : MonoBehaviour
{
    [SerializeField]
    private WindowsNavigation _windowsNavigation;
    [SerializeField]
    private GameObject _hideWindow;
    [SerializeField]
    private GameObject _showWindow;
    
    private Button _button;


    // Start is called before the first frame update
    void Start()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(WindowNavigation);
    }

    private void WindowNavigation()
    {
        _windowsNavigation.NavigatePanel(_hideWindow, _showWindow);
    }
}
