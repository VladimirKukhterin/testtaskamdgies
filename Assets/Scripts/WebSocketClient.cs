using UnityEngine;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System;
using UniRx;
using System.Net.Sockets;

public class WebSocketClient : MonoBehaviour
{
    [SerializeField] 
    private ApplicationContext _applicationContext;

    private ClientWebSocket _websocket;
    private ReactiveProperty<bool> _connectionStatus = new ReactiveProperty<bool>(false);
    private ReactiveProperty<float> _odometerValue = new ReactiveProperty<float>(0.0f);
    private ReactiveProperty<bool> _randomValue = new ReactiveProperty<bool>(false);
    public ReadOnlyReactiveProperty<float> OdometerValue => _odometerValue.ToReadOnlyReactiveProperty();
    public ReadOnlyReactiveProperty<bool> RandomValue => _randomValue.ToReadOnlyReactiveProperty();
    public ReadOnlyReactiveProperty<bool> ConnectionStatus => _connectionStatus.ToReadOnlyReactiveProperty();

    private void Start()
    {
        var config = _applicationContext.ApplicationConfig;
        Observable.Merge(config.ServerAddress.AsUnitObservable(), config.ServerPort.AsUnitObservable()).Skip(1).Subscribe(_ => Disconnect()).AddTo(this);
        ConnectWebSocket();
        Observable.Interval(TimeSpan.FromSeconds(2)).Subscribe(_ => UpdateOdometerValue()).AddTo(this);
    }

    private void Disconnect()
    {
        _websocket?.Abort();
    }

    private async void ConnectWebSocket()
    {
        _websocket = new ClientWebSocket();

        try
        {
            var config = _applicationContext.ApplicationConfig;
            Uri uri = new Uri($"ws://{config.ServerAddress.Value}:{config.ServerPort.Value}/ws");

            await _websocket.ConnectAsync(uri, CancellationToken.None);

            Debug.Log("WebSocket connected");
            _connectionStatus.Value = true;

            // ������ �������� �������� ��������
            await SendWebSocketMessage("{\"operation\": \"getCurrentOdometer\"}");

            // ������ ����� ��������� ��������� �� �������
            await ReceiveWebSocketMessages();
        }
        catch (Exception e)
        {
            var config = _applicationContext.ApplicationConfig;
            _connectionStatus.Value = false;
            Debug.LogError("WebSocket connection error: " + e.Message);
            await Task.Delay(1000);
            ConnectWebSocket();
        }
    }

    private async Task ReceiveWebSocketMessages()
    {
        byte[] buffer = new byte[1024];

        while (_websocket.State == WebSocketState.Open)
        {
            WebSocketReceiveResult result = await _websocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            string responseJson = Encoding.UTF8.GetString(buffer, 0, result.Count);
           
            // ������ JSON-������
            var response = JsonUtility.FromJson<ResponseData>(responseJson);

            if (response != null)
            {  
                // ������ 10 ������
                if (response.operation == "odometer_val")
                {
                    _odometerValue.Value = response.value;
                }
                // ���� ��������� �������� ��������
                else if (response.operation == "currentOdometer")
                {
                    _odometerValue.Value = response.odometer;
                }
                // ���� ��������� ������
                else if (response.operation == "randomStatus")
                {
                    _randomValue.Value = response.status;
                }
            }
        }

        _connectionStatus.Value = false;
        ConnectWebSocket();
    }

    public async void UpdateRandomValue()
    {
        await SendWebSocketMessage("{\"operation\": \"getRandomStatus\"}");
    }

    public async void UpdateOdometerValue()
    {
        await SendWebSocketMessage("{\"operation\": \"getCurrentOdometer\"}");
    }

    private async Task SendWebSocketMessage(string message)
    {
        byte[] buffer = Encoding.UTF8.GetBytes(message);
        await _websocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Text, true, CancellationToken.None);
    }

    private void OnDestroy()
    {
        if (_websocket != null && _websocket.State != WebSocketState.Closed)
        {
            _websocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "WebSocket close", CancellationToken.None);
        }
    }

    [Serializable]
    private class ResponseData
    {
        public string operation;
        public float value;
        public float odometer;
        public bool status;
    }
}